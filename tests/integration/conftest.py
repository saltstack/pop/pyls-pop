from unittest import mock

import pytest
from pylsp import uris
from pylsp.config.config import Config
from pylsp.workspace import Workspace


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="pyls_pop")
    hub.pop.sub.add(pypath="tests.integration.mods")
    hub.pyls_pop.cache.clear()

    with mock.patch("sys.argv", ["pylsp"]):
        hub.pop.config.load(
            ["pyls_pop"],
            cli="pyls_pop",
            parse_cli=False,
        )

    yield hub


@pytest.fixture
def workspace(tmpdir):
    """Return a workspace."""
    ws = Workspace(uris.from_fs_path(str(tmpdir)), mock.Mock())
    ws._config = Config(ws.root_uri, {}, 0, {})
    return ws


@pytest.fixture
def config(workspace):  # pylint: disable=redefined-outer-name
    """Return a config object."""
    return Config(workspace.root_uri, {}, 0, {})
