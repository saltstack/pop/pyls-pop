from pylsp import uris
from pylsp.workspace import Document

DOC_URI = uris.from_fs_path(__file__)
DOC = """
def func(hub):
    hub.mods.plugin.function(arg=1,)
"""


async def test_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 11}
    ret = hub.pyls_pop.plugin.hover(config, workspace, doc, position)
    assert ret == None


async def test_nested_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 15}
    ret = hub.pyls_pop.plugin.hover(config, workspace, doc, position)
    assert ret == {"contents": [{"language": "python", "value": "\nmods.plugin"}]}


async def test_function(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 28}
    ret = hub.pyls_pop.plugin.hover(config, workspace, doc, position)
    assert ret == {
        "contents": [
            {
                "language": "python",
                "value": "\nmods.plugin.function(hub, arg1, arg2, arg3)\n\nFunction doc",
            }
        ]
    }


async def test_signature(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 30}
    ret = hub.pyls_pop.plugin.hover(config, workspace, doc, position)
    assert ret == None
