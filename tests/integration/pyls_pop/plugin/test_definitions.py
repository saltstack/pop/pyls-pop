import pathlib

from pylsp import uris
from pylsp.workspace import Document

import tests

DOC_URI = uris.from_fs_path(__file__)
DOC = """
def func(hub):
    hub.mods.plugin.function(arg=1,)
"""
p = pathlib.Path(tests.__file__)


async def test_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 11}
    ret = hub.pyls_pop.plugin.definitions(config, doc, position)
    assert ret == []


async def test_nested_sub(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 15}
    ret = hub.pyls_pop.plugin.definitions(config, doc, position)
    assert ret == [
        {
            "range": {
                "end": {"character": 0, "line": 0},
                "start": {"character": 0, "line": 0},
            },
            "uri": f"file://{p.parent}/integration/mods/plugin.py",
        }
    ]


async def test_function(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 28}
    ret = hub.pyls_pop.plugin.definitions(config, doc, position)
    assert ret == [
        {
            "range": {
                "end": {"character": 0, "line": 5},
                "start": {"character": 0, "line": 0},
            },
            "uri": f"file://{p.parent}/integration/mods/plugin.py",
        }
    ]


async def test_exclude_declration(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 28}
    ret = hub.pyls_pop.plugin.definitions(config, doc, position)
    p = pathlib.Path(tests.__file__)
    assert ret == [
        {
            "range": {
                "end": {"character": 0, "line": 5},
                "start": {"character": 0, "line": 0},
            },
            "uri": f"file://{p.parent}/integration/mods/plugin.py",
        }
    ]


async def test_definitions(hub, config, workspace):
    await hub.pyls_pop.cache.create()
    doc = Document(DOC_URI, workspace, DOC)
    position = {"line": 2, "character": 30}
    ret = hub.pyls_pop.plugin.definitions(config, doc, position)
    assert ret == []
